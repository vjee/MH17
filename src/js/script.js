// import fetch from "isomorphic-fetch"
import fetch from "unfetch"
import Promise from "promise-polyfill"
if (!window.Promise) window.Promise = Promise

import { el, list, setStyle, setChildren, mount, unmount } from "redom"

// import { getPosts } from "./lib/api/helper.js"
import click from "./lib/click.js"

// globals /////////////////////////////////////////////////////////////////////////////////////////

let $mountPortfolio, $mountDetail
let PortfolioInstance, DetailInstance, DetailInstanceMounted

// classes /////////////////////////////////////////////////////////////////////////////////////////

class Portfolio {
  constructor() {
    this.loadPortfolio = this.loadPortfolio.bind(this)

    this.el = list("section.portfolio", Article)
    this.loadPortfolio()
  }
  loadPortfolio() {
    return fetch("/api/posts")
      .then(response => response.json())
      .then(response => response["posts"])
      .then(posts => this.el.update(posts))
      .catch(err => console.error(err))
  }
}

class Article {
  constructor() {
    this.el = el(
      "article.portfolio__item",
      el => click(el, () => handleOpenDetail(this.data)),
      (this.title = el("h2")),
      (this.description = el("p")),
      (this.date = el("span")),
      (this.images = el("div"))
    )
  }
  update(data) {
    this.data = data
    this.title.textContent = data.title
    this.description.textContent = data.description
    this.date.textContent = data.date

    const images = data.image
      .split(",")
      .map(i => el("img", { src: `/cache/img/${i}` }))
    setChildren(this.images, images)
  }
}

class Detail {
  constructor() {
    this.el = el(
      "div.detail__outer",
      { style: { opacity: 0 } },
      (this.content = el(
        "section.detail",
        (this.aside = el(
          "section.detail__aside",
          (this.title = el("h2")),
          (this.description = el("p")),
          (this.date = el("span")),
          (this.close = el(".detail__close", "X", el =>
            click(el, handleCloseDetail)
          ))
        )),
        (this.main = el("section.detail__main", (this.image = el("span"))))
      ))
    )
  }
  update(data) {
    this.title.textContent = data.title
    this.description.textContent = data.description
    this.date.textContent = data.date
    this.image.textContent = data.image
  }
  onmount() {
    // fade in
    setTimeout(() => {
      setStyle(this.el, { opacity: 1 })
    }, 100)
  }
  unmount() {
    // fade out, then unmount
    setStyle(this.el, { opacity: 0 })
    setTimeout(() => {
      unmount($mountDetail, DetailInstance)
    }, 100)
  }
}

// methods /////////////////////////////////////////////////////////////////////////////////////////

const handleOpenDetail = data => {
  closeCurrentDetail()

  DetailInstance.update(data)
  mount($mountDetail, DetailInstance)
  DetailInstanceMounted = true
}

const handleCloseDetail = () => {
  closeCurrentDetail()
}

const closeCurrentDetail = () => {
  if (DetailInstanceMounted) {
    DetailInstance.unmount()
    DetailInstanceMounted = false
  }
}

// init ////////////////////////////////////////////////////////////////////////////////////////////

const init = () => {
  // initialise globals
  $mountPortfolio = document.querySelector(".redom-mount-portfolio")
  $mountDetail = document.querySelector(".redom-mount-detail")
  PortfolioInstance = new Portfolio()
  DetailInstance = new Detail()
  DetailInstanceMounted = false

  // mount portfolio
  mount($mountPortfolio, PortfolioInstance)
}

init()
