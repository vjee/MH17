export const getPosts = () => {
  return fetch("/api/posts")
    .then(response => response.json())
    .then(response => response["posts"])
}
