const rollup = require("rollup")
const resolve = require("rollup-plugin-node-resolve")
const commonjs = require("rollup-plugin-commonjs")
const babel = require("rollup-plugin-babel")
const uglify = require("rollup-plugin-uglify")

const hash = require("./../hash")
const manifest = require("./../manifest")

module.exports = async ({ dirname, entry, dest }) => {
  const bundle = await rollup.rollup({
    entry,
    plugins: [
      resolve(),
      commonjs(),
      babel({ exclude: "node_modules/**" }),
      uglify()
    ]
  })
  console.log(entry)
  const fileHash = await hash(entry)
  const outfile = dest.replace("[hash]", fileHash)
  await bundle.write({
    dest: outfile,
    format: "iife",
    sourceMap: true
  })
  await manifest(dirname, "js", `bundle.${fileHash}.js`)
}
