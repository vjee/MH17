const Path = require("path")
const promisify = require("promisify-node")
const Fs = promisify("fs")
const Fse = require("fs-extra")

module.exports = async dirname => {
  const path = Path.join(dirname, "dist")
  const file = `${path}/MANIFEST.json`
  let result

  // ensure path exists and create if doesn't
  // prevents Fs.access from throwing
  await Fse.ensureDir(path).catch(err => console.log(err))

  // chack if file exists
  // if not, return fallback values
  result = await Fs.access(file, Fs.constants.F_OK)
    .then(() => null)
    .catch(() => ({ js: "bundle.js", css: "bundle.css" }))

  // send back result if defined
  if (result) return result

  // read the json from the file
  return await Fse.readJson(file).catch(err => console.log(err))
}
